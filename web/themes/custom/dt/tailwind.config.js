module.exports = {
  content: ["./templates/**/*.twig"],
  theme: {

    extend: {
      screens : {
        'm-sm':  {'max': '599px'}
      }
    },
  },
  plugins: [],
}
